package com.doxteam.cordova.antplus;

import android.content.Context;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeCadencePcc;
import com.dsi.ant.plugins.antplus.pcc.defines.EventFlag;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.EnumSet;

/**
 * Created by marioampov on 1/15/16.
 */
public class CCSensor extends AntPlusSensor<AntPlusBikeCadencePcc> {

    public CCSensor(Context context, CallbackContext callbackContext, int id) {
        super(context, callbackContext, id);
    }

    @Override
    public void subscribe(final CallbackContext callbackContext) {
        if (pcc == null) {
            callbackContext.error("not_connected");
            return;
        }
        pcc.subscribeRawCadenceDataEvent(new AntPlusBikeCadencePcc.IRawCadenceDataReceiver()
        {
            public void onNewRawCadenceData(final long estTimestamp,
                                            final EnumSet<EventFlag> eventFlags, final BigDecimal timestampOfLastEvent,
                                            final long cumulativeRevolutions)
            {
                JSONObject json = new JSONObject();
                try {
                    json.put("crankCumulativeRevolutions", cumulativeRevolutions);
                    json.put("timestampOfLastEvent", timestampOfLastEvent);
                    PluginResult result = new PluginResult(PluginResult.Status.OK, json);
                    result.setKeepCallback(true);
                    callbackContext.sendPluginResult(result);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public void unSubscribe(CallbackContext callbackContext) {
        if (pcc == null) {
            if (callbackContext != null) {
                callbackContext.error("not_connected");
            }
            return;
        }
        pcc.subscribeRawCadenceDataEvent(null);
        if (callbackContext != null) {
            callbackContext.success();
        }
    }

    @Override
    public void connect() {
        pccReleaseHandle = AntPlusBikeCadencePcc.requestAccess(context,
                id, 0, false, base_IPluginAccessResultReceiver, base_IDeviceStateChangeReceiver);

    }

}

