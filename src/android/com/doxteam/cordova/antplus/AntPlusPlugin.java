package com.doxteam.cordova.antplus;

import android.util.SparseArray;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceType;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaArgs;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.LOG;
import org.json.JSONException;


/**
 * Created by Mario Ampov on 12/14/15.
 * Cordova ANT+ plugin
 */
public class AntPlusPlugin extends CordovaPlugin {
    private static final String TAG = "AntPlusPlugin";

    // actions
    private static final String SCAN = "scan";
    private static final String CANCEL_SCAN = "stopScan";
    private static final String CONNECT = "connect";
    private static final String DISCONNECT = "disconnect";
    private static final String SUBSCRIBE = "subscribe";
    private static final String UN_SUBSCRIBE = "unSubscribe";
    private static final String IS_CONNECTED = "isConnected";

    private SensorSearch sensorSearch;
    private SparseArray<AntPlusSensor> sensors;

    private AntPlusSensor getSensor(int id, int service, CallbackContext callbackContext) {
        DeviceType type = DeviceType.getValueFromInt(service);
        switch (type) {
            case HEARTRATE:
                return  new HRSensor(cordova.getActivity(), callbackContext, id);
            case BIKE_CADENCE:
                return new CCSensor(cordova.getActivity(), callbackContext, id);
            case BIKE_SPD:
                return new CSDSensor(cordova.getActivity(), callbackContext, id, type);
            case BIKE_SPDCAD:
                return new CSDSensor(cordova.getActivity(), callbackContext, id, type);
        }
        return null;
    }

    @Override
    protected void pluginInitialize() {
        super.pluginInitialize();
        sensorSearch = new SensorSearch(cordova.getActivity());
        sensors = new SparseArray<AntPlusSensor>();
    }

    @Override
    public boolean execute(String action, CordovaArgs args, CallbackContext callbackContext) throws JSONException {
        LOG.d(TAG, "action = " + action);

        boolean validAction = true;

        if (action.equals(SCAN)) {
            sensorSearch.Search(callbackContext);
        } else if (action.equals(CANCEL_SCAN)) {
            sensorSearch.Close(callbackContext);
        } else {
            int id = Integer.parseInt(args.getString(0));
            AntPlusSensor sensor = sensors.get(id);
            if (action.equals(CONNECT)) {
                if (sensor == null || !sensor.isConnected()) {
                    int service = Integer.parseInt(args.getString(1));
                    AntPlusSensor newSensor = getSensor(id, service, callbackContext);
                    if (newSensor != null) {
                        sensors.put(id, newSensor);
                        newSensor.connect();
                    } else {
                        callbackContext.error("unknown service");
                    }
                } else {
                    callbackContext.error("already connected");
                }
            } else {
                if (sensor == null) {
                    callbackContext.error("unknown sensor");
                } else {
                    if (action.equals(DISCONNECT)) {
                        sensor.disconnect(callbackContext);
                    } else if (action.equals(SUBSCRIBE)) {
                        sensor.subscribe(callbackContext);
                    } else if (action.equals(UN_SUBSCRIBE)) {
                        sensor.unSubscribe(callbackContext);
                    } else if (action.equals(IS_CONNECTED)) {
                        callbackContext.success(sensor.isConnected() ? "true" : "false");
                    } else {
                        validAction = false;
                    }
                }
            }
        }

        return validAction;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        for(int i = 0; i < sensors.size(); i++) {
            int key = sensors.keyAt(i);
            sensors.get(key).release();
        }
    }
}
