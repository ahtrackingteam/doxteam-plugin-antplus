package com.doxteam.cordova.antplus;

import android.content.Context;
import com.dsi.ant.plugins.antplus.pcc.MultiDeviceSearch;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceType;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.MultiDeviceSearch.MultiDeviceSearchResult;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.EnumSet;

/**
 * Created by marioampov on 1/14/16.
 */
public class SensorSearch {

    /**
     * Relates a MultiDeviceSearchResult with an RSSI value
     */
    public class MultiDeviceSearchResultWithRSSI
    {
        public MultiDeviceSearchResult mDevice;
        public int mRSSI = Integer.MIN_VALUE;
    }

    Context mContext;
    MultiDeviceSearch mSearch;
    CallbackContext callbackContext;
    EnumSet<DeviceType> devices;
    ArrayList<MultiDeviceSearchResultWithRSSI> mFoundDevices = new ArrayList<MultiDeviceSearchResultWithRSSI>();

    public SensorSearch(Context context)
    {
        mContext = context;
        devices = EnumSet.noneOf(DeviceType.class);
        devices.add(DeviceType.HEARTRATE);
        devices.add(DeviceType.BIKE_SPDCAD);
        devices.add(DeviceType.BIKE_CADENCE);
        devices.add(DeviceType.BIKE_SPD);
    }

    public void Search(final CallbackContext callbackContext) {
        if (mSearch == null) {
            mSearch = new MultiDeviceSearch(mContext, devices, mCallback, mRssiCallback);
            this.callbackContext = callbackContext;
        } else  {
            callbackContext.error("search in progress");
        }
    }

    public void Close(final CallbackContext callbackContext)
    {
        if (mSearch != null) {
            mSearch.close();
            mSearch = null;
            mFoundDevices.clear();
            callbackContext.success();
        } else {
            callbackContext.error("search not started");
        }
    }

    private JSONObject multiDeviceSearchResultToJSON(MultiDeviceSearchResultWithRSSI searchResultWithRSSI)
    {
        JSONObject json = new JSONObject();
        try {
            json.put("name", searchResultWithRSSI.mDevice.getDeviceDisplayName());
            json.put("id", Integer.toString(searchResultWithRSSI.mDevice.getAntDeviceNumber()));
            json.put("service", Integer.toString(searchResultWithRSSI.mDevice.getAntDeviceType().getIntValue()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    /**
     * Callbacks from the multi-device search interface
     */
    private MultiDeviceSearch.SearchCallbacks mCallback = new MultiDeviceSearch.SearchCallbacks()
    {
        /**
         * Called when a device is found. Display found devices in connected and
         * found lists
         */
        public void onDeviceFound(final MultiDeviceSearchResult deviceFound)
        {
            final MultiDeviceSearchResultWithRSSI result = new MultiDeviceSearchResultWithRSSI();
            result.mDevice = deviceFound;
            if (!deviceFound.isAlreadyConnected())
            {
                mFoundDevices.add(result);
                PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, multiDeviceSearchResultToJSON(result));
                pluginResult.setKeepCallback(true);
                callbackContext.sendPluginResult(pluginResult);
            }
        }

        /**
         * The search has been stopped unexpectedly
         */
        public void onSearchStopped(RequestAccessResult reason)
        {
            callbackContext.error(reason.toString());
            mSearch = null;
        }

        public void onSearchStarted(MultiDeviceSearch.RssiSupport supportsRssi)
        {
        }
    };

    /**
     * Callback for RSSI data of previously found devices
     */
    private MultiDeviceSearch.RssiCallback mRssiCallback = new MultiDeviceSearch.RssiCallback()
    {
        /**
         * Receive an RSSI data update from a specific found device
         */
        public void onRssiUpdate(final int resultId, final int rssi)
        {
            for (MultiDeviceSearchResultWithRSSI result : mFoundDevices)
            {
                if (result.mDevice.resultID == resultId)
                {
                    /**
                     * TODO: We should make a callback to js for updated RSSI.
                     */
                    result.mRSSI = rssi;
                    break;
                }
            }
        }
    };

}
