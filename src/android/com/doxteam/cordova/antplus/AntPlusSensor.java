package com.doxteam.cordova.antplus;

import android.content.Context;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.*;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;

/**
 * Created by Mario Ampov on 12/14/15.
 * Abstract implementation of ant+ sensor.
 */
public abstract class AntPlusSensor<T extends AntPlusLegacyCommonPcc> {

    Context context;
    CallbackContext connectContext;
    int id;
    T pcc = null;
    PccReleaseHandle<T> pccReleaseHandle = null;

    public abstract void subscribe(CallbackContext callbackContext);

    public abstract void unSubscribe(CallbackContext callbackContext);

    public abstract void connect();

    public AntPlusSensor(Context context, CallbackContext callbackContext, int id) {
        this.context = context;
        this.connectContext = callbackContext;
        this.id = id;
    }

    protected void handleError(CallbackContext callbackContext, RequestAccessResult resultCode) {
        switch (resultCode) {
            case CHANNEL_NOT_AVAILABLE:
                callbackContext.error("channel_not_available");
                break;
            case ADAPTER_NOT_DETECTED:
                callbackContext.error("adapter_not_detected");
                break;
            case BAD_PARAMS:
                callbackContext.error("channel_not_available");
                break;
            case OTHER_FAILURE:
                callbackContext.error("other_failure");
                break;
            case DEPENDENCY_NOT_INSTALLED:
                callbackContext.error("dependency_not_installed");
                break;
            case USER_CANCELLED:
                callbackContext.error("user_cancelled");
                break;
            case UNRECOGNIZED:
                callbackContext.error("unrecognized");
                break;
            case SEARCH_TIMEOUT:
                callbackContext.error("timeout");
                break;
            default:
                callbackContext.error("unrecognized");
                break;
        }
    }

    protected AntPluginPcc.IDeviceStateChangeReceiver base_IDeviceStateChangeReceiver =
            new AntPluginPcc.IDeviceStateChangeReceiver() {
                public void onDeviceStateChange(final DeviceState newDeviceState) {
                    if (newDeviceState == DeviceState.DEAD) {
                        pccReleaseHandle.close();
                        pccReleaseHandle = null;
                        pcc = null;
                        if (connectContext != null) {
                            connectContext.error("disconnected");
                        }
                    }
                }
            };

    protected AntPluginPcc.IPluginAccessResultReceiver<T> base_IPluginAccessResultReceiver =
            new AntPluginPcc.IPluginAccessResultReceiver<T>() {
                public void onResultReceived(T result,
                                             RequestAccessResult resultCode, DeviceState initialDeviceState) {
                    if (resultCode == RequestAccessResult.SUCCESS) {
                        pcc = result;
                        PluginResult pluginResult = new PluginResult(PluginResult.Status.OK);
                        pluginResult.setKeepCallback(true);
                        connectContext.sendPluginResult(pluginResult);
                    } else {
                        handleError(connectContext, resultCode);
                    }
                }
            };

    public boolean isConnected() {
        return pcc != null;
    }

    public void disconnect(final CallbackContext callbackContext) {
        connectContext = null;
        if (pccReleaseHandle != null) {
            pccReleaseHandle.close();
            pccReleaseHandle = null;
            pcc = null;
        }
        if (callbackContext != null) {
            callbackContext.success();
        }
    }

    public void release() {
        if (pccReleaseHandle != null) {
            pccReleaseHandle.close();
        }
    }

}
