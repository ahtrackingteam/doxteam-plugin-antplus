package com.doxteam.cordova.antplus;

import android.content.Context;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeSpeedDistancePcc;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceType;
import com.dsi.ant.plugins.antplus.pcc.defines.EventFlag;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.EnumSet;

/**
 * Created by marioampov on 1/15/16.
 */
public class CSDSensor extends AntPlusSensor<AntPlusBikeSpeedDistancePcc> {

    DeviceType type;
    CCSensor ccSensor;

    public CSDSensor(Context context, CallbackContext callbackContext, int id, DeviceType type) {
        super(context, callbackContext, id);
        this.type = type;
    }

    protected AntPluginPcc.IPluginAccessResultReceiver<AntPlusBikeSpeedDistancePcc> csdc_IPluginAccessResultReceiver =
            new AntPluginPcc.IPluginAccessResultReceiver<AntPlusBikeSpeedDistancePcc>() {
                public void onResultReceived(AntPlusBikeSpeedDistancePcc result,
                                             RequestAccessResult resultCode, DeviceState initialDeviceState) {
                    if (resultCode == RequestAccessResult.SUCCESS) {
                        pcc = result;
                        ccSensor = new CCSensor(context, connectContext, id);
                        ccSensor.connect();
                    } else {
                        handleError(connectContext, resultCode);
                    }
                }
            };

    @Override
    public void subscribe(final CallbackContext callbackContext) {
        if (pcc == null) {
            callbackContext.error("not_connected");
            return;
        }
        pcc.subscribeRawSpeedAndDistanceDataEvent(new AntPlusBikeSpeedDistancePcc.IRawSpeedAndDistanceDataReceiver()
        {
            public void onNewRawSpeedAndDistanceData(final long estTimestamp,
                                                     final EnumSet<EventFlag> eventFlags,
                                                     final BigDecimal timestampOfLastEvent, final long cumulativeRevolutions)
            {
                JSONObject json = new JSONObject();
                try {
                    json.put("wheelCumulativeRevolutions", cumulativeRevolutions);
                    json.put("timestampOfLastEvent", timestampOfLastEvent);
                    PluginResult result = new PluginResult(PluginResult.Status.OK, json);
                    result.setKeepCallback(true);
                    callbackContext.sendPluginResult(result);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        if (type == DeviceType.BIKE_SPDCAD && ccSensor != null && ccSensor.isConnected()) {
            ccSensor.subscribe(callbackContext);
        }

    }

    @Override
    public void unSubscribe(CallbackContext callbackContext) {
        if (pcc == null) {
            callbackContext.error("not_connected");
            return;
        }
        pcc.subscribeRawSpeedAndDistanceDataEvent(null);
        if (type == DeviceType.BIKE_SPDCAD && ccSensor != null && ccSensor.isConnected()) {
            ccSensor.unSubscribe(callbackContext);
        } else {
            callbackContext.success();
        }
    }

    @Override
    public void connect() {
        switch (type) {
            case BIKE_SPD:
                pccReleaseHandle = AntPlusBikeSpeedDistancePcc.requestAccess(context,
                        id, 0, false, base_IPluginAccessResultReceiver, base_IDeviceStateChangeReceiver);
                break;
            case BIKE_SPDCAD:
                pccReleaseHandle = AntPlusBikeSpeedDistancePcc.requestAccess(context,
                        id, 0, true, csdc_IPluginAccessResultReceiver, base_IDeviceStateChangeReceiver);
        }

    }

    @Override
    public void release() {
        super.release();
        if (ccSensor != null) {
            ccSensor.release();
        }
    }
}
