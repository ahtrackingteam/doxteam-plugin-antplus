package com.doxteam.cordova.antplus;

import android.content.Context;
import com.dsi.ant.plugins.antplus.pcc.AntPlusHeartRatePcc;
import com.dsi.ant.plugins.antplus.pcc.defines.EventFlag;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;

import java.math.BigDecimal;
import java.util.EnumSet;

/**
 * Created by Mario Ampov on 12/14/15.
 * Implementation of heart rate sensor.
 */
public class HRSensor extends AntPlusSensor<AntPlusHeartRatePcc> {

    public HRSensor(Context context, CallbackContext callbackContext, int id) {
        super(context, callbackContext, id);
    }

    @Override
    public void subscribe(final CallbackContext callbackContext) {
        if (pcc == null) {
            callbackContext.error("not_connected");
            return;
        }
        pcc.subscribeHeartRateDataEvent(new AntPlusHeartRatePcc.IHeartRateDataReceiver() {
            public void onNewHeartRateData(final long estTimestamp, EnumSet<EventFlag> eventFlags,
                                           final int computedHeartRate, final long heartBeatCount,
                                           final BigDecimal heartBeatEventTime, final AntPlusHeartRatePcc.DataState dataState) {
                PluginResult result = new PluginResult(PluginResult.Status.OK, computedHeartRate);
                result.setKeepCallback(true);
                callbackContext.sendPluginResult(result);
            }
        });
    }

    @Override
    public void unSubscribe(CallbackContext callbackContext) {
        if (pcc == null) {
            callbackContext.error("not_connected");
            return;
        }
        pcc.subscribeHeartRateDataEvent(null);
        callbackContext.success();
    }

    @Override
    public void connect() {
        pccReleaseHandle = AntPlusHeartRatePcc.requestAccess(context, id, 0,
                base_IPluginAccessResultReceiver, base_IDeviceStateChangeReceiver);
    }
}
