var exec = require('cordova/exec');


module.exports = {

    scan: function (success, failure) {
        exec(success, failure, 'AntPlus', 'scan', []);
    },

    stopScan: function (success, failure) {
        exec(success, failure, 'AntPlus', 'stopScan', []);
    },

    connect: function (id, service, success, failure) {
        exec(success, failure, 'AntPlus', 'connect', [id, service]);
    },

    disconnect: function (id, success, failure) {
        exec(success, failure, 'AntPlus', 'disconnect', [id]);
    },

    subscribe: function (id, success, failure) {
        exec(success, failure, 'AntPlus', 'subscribe', [id]);
    },

    unSubscribe: function (id, success, failure) {
        exec(success, failure, 'AntPlus', 'unSubscribe', [id]);
    },

    isConnected: function (id, success, failure) {
        exec(success, failure, 'AntPlus', 'isConnected', [id]);
    },


};
